# GTFS-OSM validator

Purpose of this tool is to help find errors and differences between GTFS and OSM data. It will also help point to
errors in PTv2 routes in OSM.

Once you import data in OSM (or when new version of GTFS data is released),
you can use this tool to check if all data from GTFS is imported in OSM correctly
and what are differences.

Tool is heavily relying on PTv2 spec and do not try to deviate from it.

All values in tool are having sensible defaults to cover broad range of GTFS datasets and their
usages in OSM, but there is no way this tool perfectly work in each case, so it is expected to
tweak this tool here and there - **feel free to ask for help!**

This tool is not general-purpose PTv2 quality assurance tool, even though it points to a lot of PTv2 errors in OSM.
It is handy if you are OK with console output and quick iterations. Check
[this wiki](https://wiki.openstreetmap.org/wiki/Public_transport/Quality_Assurance)
for other great tools.

## How it looks like

Currently, all issues are printed in console.

Here is [one small example output](https://gitlab.com/stalker314314/gtfs-osm-validator/-/snippets/2046260)
from one route master in Belgrade, Serbia and [here is another big one](https://gitlab.com/stalker314314/gtfs-osm-validator/-/snippets/2046258)
from Zagreb, Croatia.

## How it works

Program is first getting all route masters in OSM based on GTFS data (or whatever else how you specify
your route masters) using Overpass. It then check hierarchically route masters, then routes and then stops.
You might need to define how to obtain route masters (see Usage), but after that point, it will automatically
match OSM route masters with GTFS routes, it will crawl route masters to get routes, check tags in routes and
try desperately to get one sample of trip_id to check stops. To do that, it will use `gtfs:shape_id`,
`gtfs:trip_id:sample` or some heuristic based on `from` and `to` tags. It will try from more correct and specific
cases (`gtfs:shape_id`) to more probabilistic ("this direction seems more correct, let's go with it"). It will
print any warnings on what it is trying, so you can help it by defining better tags. It will also check proper
geometries (how much OSM and GTFS deviates in meters), whether there are gaps that can be corrected by reordering,
is there oneway streets in wrong way...

Finally, it will crawl all stops and check tags and geometry in these. Since each trip in GTFS can have its own
set of stops, tool will try its best to find proper `trip_id` to use. It will try using
`gtfs:trip_id:sample`/`gtfs:trip_id` if those exists, and if they do not, it will try to query GTFS
for trip that in defined `shape_id` and _best_ service. "Best" service is heuristically chosen as the one
that is in operation in the moment when this script is run (if it is summer now, and there are summer and winter
services, it will try taking summer one). If there is none such, tool will try to guess direction from `from` and
`to` stops. So, it is trying from more correct ways to more probabilistic ones, trying really hard. It will print
any warnings on what it is trying, so you can help it by defining better tags. Once "best" service is found,
tool will take a first trip in that service and shape to get sample trip to check stops. Besides tags, it will check
various geometry problems with stops (whether stops are not too far away between OSM and GTFS, whether they are not too
far away from route itself, whether they are on proper side of the road...)

Upon first boot, GTFS data will be imported into local SQLite database (so first boot might be
slower) named `gtfs.sqlite` and this file will be used in subsequent runs. 
 
## Install

Since tool is using gtfspy (which unfortunately is heavily using Pandas, numpy etc. libraries),
you are encouraged to use Linux.

There is no real installation, you need to clone repo locally with:
```shell script
git clone
cd 
```

And then install all needed requirements:
```
pip install -r requirements.txt 
```

I noticed there are some errors during `gtfspy` pip installation on Debian-based distros, but funnily all works.

## Usage

Before first usage, please check `config.py`. Go over it, most importantly check value of
`ROUTE_MASTER_QUERY_METHOD`, as this is where it all starts. Sensible default is to use agency name to get route masters.
But there is high probability that GTFS data for agency name and `gtfs:agency_name` in route masters
(if that exists at all) are not same, so you will need to experiment on best method to query these. There are some
methods to get route masters in config, but I am sure there will be need for more in future.

Then just run it with:
```shell script
python src/main.py
```

Program will print out all errors and differences it found in GTFS-OSM. You can define now what
you want to fix and how.

# Limitations

* Only bus, trams and trolleybus are supported

Feel free to open issue if you want something from list above resolved, or you hit some other limitations.

# Things checked

Here is a list of all stuff in OSM that this tool can check from GTFS data.

## Route master

### Tag checks

* `name`
* `route_master`
* `gtfs:route_id`
* `gtfs:agency_id`
* `gtfs:agency_url`
* `operator`
* `network`
* `colour`

### Other

* Does OSM have all variants of routes from GTFS

## Route

### Tag checks

* `public_transport:version`
* `route`
* `gtfs:route_id`
* `gtfs:shape_id`
* `gtfs:trip_id:sample`/`gtfs:trip_id`
* `from`
* `to`
* `gtfs:agency_id`
* `gtfs:agency_url`
* `duration`
* `interval`
* `operator`
* `network`
* `colour`
* `roundtrip`

### Geometry checks

* Route shape difference in meters between OSM and GTFS (Hausdorff distance)
* Presence of gaps in OSM route
  * Whether gaps can be corrected by reordering ways
* Oneway roads along the OSM route in wrong directions
* Same roundabouts mentioned multiple times
* Difference in total length of OSM and GTFS route

## Stops

### Tag checks

* `name`
* `highway`
* `bus`/`tram`
* `ref`/`gtfs_id`/`gtfs:stop_id`
* `operator`
* `network`

### Geometry checks

* Distance between stop in OSM and GTFS in meters
* Distance from OSM stop and OSM route
* Side of the road between OSM stop and GTFS stop
* Check that platform is not part of the way/check that stop is part of the way

### Other

* Check that all stop that have proper operator tag have routes on them with that operator tag