import argparse
import datetime
import os
from itertools import zip_longest
import re
import importlib

import overpy
import pyproj
from gtfspy.route_types import ROUTE_TYPE_TO_LOWERCASE_TAG

import gtfs_queries
import osm_queries
from helpers import load_or_import_example_gtfs
from geometry import GeometryUtils

geod = pyproj.Geod(ellps='WGS84')

# Regex to represent "ref: from => (via => ...) to"
route_name_regex = re.compile("(.*):(.*?)(=>.*)+")


def process_stops(context, osm_route, osm_route_data, geom_osm_route, gtfs_stops, gtfs_route_type, geom_gtfs_route):
    config = context['config']
    geometry_utils = context['geometry_utils']

    osm_stops = []
    # First we need to decide should we do checks on platforms or on stops. For this, we will count both and decide
    use_platform = False
    platform_count = len([m for m in osm_route.members if m.role in ('platform', 'platform_entry_only', 'platform_exit_only')])
    stop_count = len([m for m in osm_route.members if m.role in ('stop', 'stop_entry_only', 'stop_exit_only')])
    if platform_count > stop_count:
        use_platform = True

    for member in osm_route.members:
        if use_platform and member.role != "platform" and member.role != "platform_entry_only" and member.role != "platform_exit_only":
            continue
        if not use_platform and member.role != "stop" and member.role != "stop_entry_only" and member.role != "stop_exit_only":
            continue
        if type(member) == overpy.RelationNode:
            osm_stops.append(next(n for n in osm_route_data.nodes if n.id == member.ref))
        elif type(member) == overpy.RelationWay:
            osm_stops.append(next(n for n in osm_route_data.ways if n.id == member.ref))
        elif type(member) == overpy.RelationRelation:
            osm_stops.append(next(n for n in osm_route_data.relations if n.id == member.ref))
        else:
            raise Exception("Unknown type {0} found, expected node, way or relation".format(str(type(member))))
    print("    Processing stops for OSM route {0} (using {1} for all checks)".format(
        osm_route.id, 'platforms' if use_platform else 'stops'))

    # Check if platform is not on a way of route (or that stop is on the way of route)
    all_route_ways = [w for w in osm_route.members if w.role == '' and type(w) == overpy.RelationWay]
    all_nodes_in_route = []
    for all_route_way in all_route_ways:
        found_way = next(w for w in osm_route_data.ways if w.id == all_route_way.ref)
        all_nodes_in_route += [n.id for n in found_way.nodes]

    for osm_stop in osm_stops:
        if type(osm_stop) != overpy.Node:
            continue
        if use_platform and osm_stop.id in all_nodes_in_route:
            print("      Platform {0} ({1}) should not be part of the route way".format(
                osm_stop.tags['name'] if 'name' in osm_stop.tags else '', osm_stop.id))
        if not use_platform and osm_stop.id not in all_nodes_in_route:
            print("      Stop {0} ({1}) should be part of the route way".format(
                osm_stop.tags['name'] if 'name' in osm_stop.tags else '', osm_stop.id))

    # Check if number of stops match (if they don't, we cannot compare them)
    if gtfs_stops.shape[0] != len(osm_stops):
        print("      Number of stops in OSM route {0} ({1}) is not same as number of stops in GTFS ({2})".format(
            osm_route.id, len(osm_stops), gtfs_stops.shape[0]))
        print("      GTFS stop name (stop_id) - OSM stop name (OSM id)")
        for gtfs_stop, osm_stop in zip_longest(gtfs_stops.iterrows(), osm_stops):
            osm_stop_id, osm_stop_name = 'N/A', ''
            gtfs_stop_id, gtfs_stop_name = 'N/A', ''
            if osm_stop:
                osm_stop_name = osm_stop.tags['name'] if 'name' in osm_stop.tags else ''
                osm_stop_id = osm_stop.id
            if gtfs_stop:
                gtfs_stop_id = gtfs_stop[1]['stop_id'] if gtfs_stop[1]['stop_id'] else 'N/A'
                gtfs_stop_name = gtfs_stop[1]['name'] if gtfs_stop[1]['name'] else ''
            print("      {0} ({1}) - {2} ({3})".format(
                gtfs_stop_name, gtfs_stop_id, osm_stop_name, osm_stop_id))
        print("      Cannot proceed with stop checks")
        return

    # From this point on, we will assume that order in OSM is OK.
    stop_idx = 0
    for osm_stop, gtfs_stop in zip(osm_stops, gtfs_stops.iterrows()):
        stop_idx = stop_idx + 1
        osm_stop_name = osm_stop.tags['name'] if 'name' in osm_stop.tags else ''
        # Check public_transport
        if 'public_transport' in osm_stop.tags:
            if use_platform and osm_stop.tags['public_transport'] != 'platform':
                print("      Stop {0} ({1}) should have public_transport=platform".format(osm_stop_name, osm_stop.id))
            if not use_platform and osm_stop.tags['public_transport'] != 'stop_position':
                print("      Stop {0} ({1}) should have public_transport=stop_position".format(osm_stop_name, osm_stop.id))
        # Check name
        gtfs_stop_name = config.NORMALIZE_GTFS_NAME(gtfs_stop[1]["name"])
        if 'name' not in osm_stop.tags:
            print("      Stop {0} ({1}) does not have name and it should be {2}".format(
                osm_stop_name, osm_stop.id, gtfs_stop_name))
        else:
            if config.NORMALIZE_OSM_NAME(osm_stop.tags['name']) != gtfs_stop_name:
                print("      Name of stop {0} ({1}) in OSM ({2}) and in GTFS ({3}) are different "
                      "(check if they are different stops altogether)".format(
                            osm_stop_name, osm_stop.id, osm_stop.tags['name'], gtfs_stop_name))
        # Check route type
        if gtfs_route_type[0] == 'bus':
            if 'highway' not in osm_stop.tags or osm_stop.tags['highway'] != 'bus_stop':
                print("      Stop {0} ({1}) should have highway=bus_stop".format(osm_stop_name, osm_stop.id))
            has_bus = 'bus' in osm_stop.tags and osm_stop.tags['bus'] == 'yes'
            has_trolleybus = 'trolleybus' in osm_stop.tags and osm_stop.tags['trolleybus'] == 'yes'
            if not (has_bus or has_trolleybus):
                print("      Stop {0} ({1}) should have either bus=yes or trolleybus=yes".format(osm_stop_name, osm_stop.id))
        elif gtfs_route_type[0] == 'tram':
            if 'railway' not in osm_stop.tags or osm_stop.tags['railway'] != 'tram_stop':
                print("      Stop {0} ({1}) should have railway=tram_stop".format(osm_stop_name, osm_stop.id))
            if 'tram' not in osm_stop.tags or osm_stop.tags['tram'] != 'yes':
                print("      Stop {0} ({1}) should have tram=yes".format(osm_stop_name, osm_stop.id))
        else:
            # TODO: support other types
            pass
        # Check stop ids
        for gtfs_stop_key in config.GTFS_STOP_KEY:
            if gtfs_stop_key not in osm_stop.tags:
                print("      Stop {0} ({1}) does not have {2} and it should be {3}".format(
                    osm_stop_name, osm_stop.id, gtfs_stop_key, gtfs_stop[1]["stop_id"]))
            else:
                if osm_stop.tags[gtfs_stop_key] != gtfs_stop[1]["stop_id"]:
                    print("      {0} of stop {1} ({2}) in OSM ({3}) and in GTFS ({4}) are different".format(
                        gtfs_stop_key, osm_stop_name, osm_stop.id, osm_stop.tags[gtfs_stop_key],
                        gtfs_stop[1]["stop_id"]))
        # Check network, operator
        if config.OPERATOR:
            if 'operator' not in osm_stop.tags or osm_stop.tags['operator'] != config.OPERATOR:
                print("      Stop {0} ({1}) should have operator={2}".format(
                    osm_stop_name, osm_stop.id, config.OPERATOR))
        if config.NETWORK:
            if 'network' not in osm_stop.tags or osm_stop.tags['network'] != config.NETWORK:
                print("      Stop {0} ({1}) should have network={2}".format(osm_stop_name, osm_stop.id, config.NETWORK))
        # Check distance between stop in OSM and in GTFS
        # TODO: check distance if stop is way too
        # TODO: don't check distance if ref of gtfs:stop_id do not match
        if type(osm_stop) == overpy.Node:
            _, _, distance = geod.inv(osm_stop.lat, osm_stop.lon, gtfs_stop[1]["lat"], gtfs_stop[1]["lon"])
            if distance >= config.DISTANCE_STOPS_METERS_THRESHOLD:
                print("      Stop {0} ({1}) is {2:.1f}m far from GTFS ({3}) stop".format(
                    osm_stop_name, osm_stop.id, distance, gtfs_stop[1]["name"]))

        # Check distance of OSM stop to OSM route
        if geom_osm_route and type(osm_stop) == overpy.Node:
            # TODO: check distance even if stop is way too
            stop_distance_from_route = geometry_utils.get_stop_distance(osm_stop.lon, osm_stop.lat, geom_osm_route)
            if stop_distance_from_route > config.DISTANCE_STOP_FROM_ROUTE_METERS_THRESHOLD:
                print("      Stop {0} ({1}) is {2:.1f}m far from its route".format(
                    osm_stop_name, osm_stop.id, stop_distance_from_route))

        # Calculate if stop is on left or right side of route shape
        if stop_idx != 1 and stop_idx != len(osm_stops):
            # Skip first and last stop, they are too close to geometry end and they are flaky
            if geom_osm_route and geom_gtfs_route and type(osm_stop) == overpy.Node:
                # TODO: check distance even if stop is way too
                right_side_osm = geometry_utils.is_stop_on_right_side(osm_stop.lon, osm_stop.lat, geom_osm_route)
                right_side_gtfs = geometry_utils.is_stop_on_right_side(gtfs_stop[1]['lon'], gtfs_stop[1]['lat'], geom_gtfs_route)
                if right_side_osm and right_side_gtfs and right_side_osm != right_side_gtfs:
                    print("      Stop {0} ({1}) is on the the {2} side looking at OSM geometry and on the {3} side "
                          "looking at GTFS shape".format(
                            osm_stop_name, osm_stop.id,
                            'right' if right_side_osm else 'left', 'right' if right_side_gtfs else 'left'))


def determine_trip_id_for_route(context, osm_route, gtfs_route, gtfs_trips):
    """
    Based on OSM route we are looking at, determine what is direction of that route and find most
    suitable trip_id.
    1. gtfs:trip_id - we first try direct read from gtfs:trip_id or gtfs:trip_id:sample (depending if route has one
    or more than one trip).
    2. gtfs:shape_id - if this doesn't work, we will query gtfs:shape_id and choose trip_id for a given shape_id
    2. a. We will first check most common first and last stop (as sometimes first/last trip during the day have
    different stops) and choose trip with most common first/last stops. If this doesn't work
    2. b. We will chose random trip_id for a given service_id and shape_id (with disregard for first/last stops)
    3. from/to - if this doesn't work, we will take 'from' and 'to' values in OSM route and try to find directions based
    on that. Since we need direction, we will try to guess it based on most common first and last stops from GTFS and
    match those to 'from' and 'to' values from OSM
    """
    config = context['config']
    most_appropriate_service = context['gtfs_service']
    # Check that there should be either gtfs:trip_id or gtfs:trip_id:sample based on how many trips there are
    if gtfs_trips.shape[0] == 1:
        # We expect gtfs:trip_id
        gtfs_trip_id = gtfs_trips.iloc[0]['trip_id']
        if 'gtfs:trip_id' not in osm_route.tags:
            print("    gtfs:trip_id is missing for route {0} (it has only one trip) and it should be {1}".format(
                osm_route.id, gtfs_trip_id))
        else:
            if osm_route.tags['gtfs:trip_id'] != gtfs_trip_id:
                print("    gtfs:trip_id for route {0} in OSM ({1}) and in GTFS ({2}) are different".format(
                    osm_route.id, osm_route.tags['gtfs:trip_id'], gtfs_trip_id))
            else:
                osm_trip_id = osm_route.tags['gtfs:trip_id']
                return osm_trip_id
    elif gtfs_trips.shape[0] > 1:
        # We expect gtfs_trip_id:sample
        if 'gtfs:trip_id:sample' not in osm_route.tags:
            # TODO: suggest trip_id for user
            print("    gtfs:trip_id:sample is missing for route {0} (GTFS has multiple trips)".format(osm_route.id))
        else:
            osm_trip_id = osm_route.tags['gtfs:trip_id:sample']
            if gtfs_trips.query("trip_id=='{0}'".format(osm_trip_id)).shape[0] == 0:
                print("    gtfs:trip_id:sample for route {0} in OSM ({1}) does not exist in GTFS".format(
                    osm_route.id, osm_route.tags['gtfs:trip_id:sample']))
            else:
                return osm_trip_id

    print("    WARNING: Check for stops will proceed with randomly chosen trip from gtfs:shape_id")
    if 'gtfs:shape_id' in osm_route.tags:
        from_stop = gtfs_queries.get_gtfs_first_stop_with_shape(
            context['gtfs_db'], gtfs_route['route_I'], osm_route.tags['gtfs:shape_id'])
        to_stop = gtfs_queries.get_gtfs_last_stop_with_shape(
            context['gtfs_db'], gtfs_route['route_I'], osm_route.tags['gtfs:shape_id'])
        possible_trips = gtfs_queries.get_gtfs_trip_by_first_and_last_stop(
            context['gtfs_db'], gtfs_route['route_I'], most_appropriate_service['service_I'],
            shape_id=osm_route.tags['gtfs:shape_id'], direction_id=None, first_stop=from_stop, last_stop=to_stop)
        if possible_trips.shape[0] == 0:
            print("    WARNING: there are no trips for a chosen service {service_id}, shape {shape_id} and most common "
                  "first stop from GTFS ('{from_stop}') and most common last stop ('{to_stop}'). "
                  "Will try choosing trip without stops.".format(
                    service_id=most_appropriate_service['service_I'], shape_id=osm_route.tags['gtfs:shape_id'],
                    from_stop=from_stop, to_stop=to_stop))
            possible_trips = gtfs_trips[
                (gtfs_trips['service_I'] == most_appropriate_service['service_I']) &
                (gtfs_trips['shape_id'] == osm_route.tags['gtfs:shape_id'])]
            if possible_trips.shape[0] == 0:
                print("    WARNING: there are no trips for a chosen service {service_id} and a shape {shape_id}, "
                      "cannot determine trip".format(
                        service_id=most_appropriate_service['service_id'], shape_id=osm_route.tags['gtfs:shape_id']))
            else:
                osm_trip_id = possible_trips.sort_values('trip_id').iloc[0]['trip_id']
                print("    NOTE: Random chosen trip to check stops is {trip_id}".format(trip_id=osm_trip_id))
                return osm_trip_id
        else:
            osm_trip_id = possible_trips.sort_values('trip_id').iloc[0]['trip_id']
            print("    NOTE: Random chosen trip to check stops is {trip_id}".format(trip_id=osm_trip_id))
            return osm_trip_id

    print("    WARNING: There is no gtfs:shape_id. "
          "Without it, check for stops will proceed with 'from' and 'to' values")
    if 'from' in osm_route.tags and 'to' in osm_route.tags:
        from_stop = config.NORMALIZE_OSM_NAME(osm_route.tags['from'])
        to_stop = config.NORMALIZE_OSM_NAME(osm_route.tags['to'])
        first_stop_per_direction = gtfs_queries.get_gtfs_first_stop(context['gtfs_db'], gtfs_route['route_I'])
        last_stop_per_direction = gtfs_queries.get_gtfs_last_stop(context['gtfs_db'], gtfs_route['route_I'])
        first_and_last_per_direction = first_stop_per_direction.join(last_stop_per_direction, on='direction_id',
                                                                     lsuffix='_first', rsuffix='_last')
        # Now that we have map direction->(first stop, last stop), try to see if any direction matches
        found_direction = None
        for direction in first_and_last_per_direction.iterrows():
            if config.NORMALIZE_GTFS_NAME(direction[1]['name_first']) == from_stop and \
                    config.NORMALIZE_GTFS_NAME(direction[1]['name_last']) == to_stop:
                found_direction = direction[0]
                break
        if found_direction:
            gtfs_possible_proper_trips = gtfs_queries.get_gtfs_trip_by_first_and_last_stop(
                context['gtfs_db'], gtfs_route['route_I'], most_appropriate_service['service_I'],
                shape_id=None, direction_id=found_direction, first_stop=from_stop, last_stop=to_stop)
            if gtfs_possible_proper_trips.shape[0] == 0:
                print("    WARNING: No trips found for first ({0}) and last ({1}) stop".format(from_stop, to_stop))
            else:
                gtfs_proper_trip_id = gtfs_possible_proper_trips.sort_values('trip_id').iloc[0]['trip_id']
                print("    NOTE: Random chosen trip to check stops based on from and to is {trip_id}".format(
                    trip_id=gtfs_proper_trip_id))
                return gtfs_proper_trip_id
        else:
            print("    WARNING: no first ({0}) and last ({1}) stop names in OSM route {0} matched any first and "
                  "last stops from GTFS, possible candidates were:".format(from_stop, to_stop, osm_route.id))
            for direction in first_and_last_per_direction.iterrows():
                print("      GTFS direction: {0}, first stop: {1}, last_stop: {2}".format(
                    direction[0], direction[1]['name_first'], direction[1]['name_last']))
    else:
        print("    WARNING: from and to tags not found in OSM route, cannot check stops in any way, giving up")
    return None


def check_route_connectivity(osm_route, osm_route_data):
    # Check connectivity
    previous_way = None
    gaps_found = 0
    for member in osm_route.members:
        if member.role != "":
            continue
        if type(member) != overpy.RelationWay:
            print("    Route member ({0}) without role which is not a route way found in route".format(member.ref))
            continue
        current_way = next(w for w in osm_route_data.ways if w.id == member.ref)
        if not previous_way:
            # This is first way, just set it
            previous_way = current_way
            continue
        # Eligible nodes that needs to be checked if they are inter-connected.
        # If it is open way, those are first and last nodes in a way.
        # If it is closed way, those are all nodes in a way.
        eligible_previous_nodes = [previous_way.nodes[0], previous_way.nodes[-1]]
        eligible_current_nodes = [current_way.nodes[0], current_way.nodes[-1]]
        current_way_closed = len(current_way.nodes) > 1 and current_way.nodes[0].id == current_way.nodes[-1].id
        previous_way_closed = len(previous_way.nodes) > 1 and previous_way.nodes[0].id == previous_way.nodes[-1].id
        if current_way_closed:
            eligible_current_nodes = current_way.nodes
            if previous_way.id == current_way.id:
                print('    Same roundabout {0} ({1}) repeated needlessly in route'.format(
                    current_way.tags['name'] if 'name' in current_way.tags else '', current_way.id
                ))
        if previous_way_closed:
            eligible_previous_nodes = previous_way.nodes

        found_any_connected = False
        for previous_node in eligible_previous_nodes:
            same_nodes = [n for n in eligible_current_nodes if n.id == previous_node.id]
            found_any_connected = found_any_connected or (len(same_nodes) > 0)
            if found_any_connected:
                break
        if not found_any_connected:
            print('    Found gap - way {0} ({1}, {2} nodes) which is not connected to previous way'.format(
                current_way.tags['name'] if 'name' in current_way.tags else '', current_way.id, len(current_way.nodes)
            ))
            gaps_found = gaps_found + 1
        previous_way = current_way
    if gaps_found > 0:
        print('    Found {0} gap{1} in route'.format(gaps_found, 's' if gaps_found > 1 else ''))
    return gaps_found == 0


def process_route(context, osm_route, osm_route_data, gtfs_route):
    """
    Returns trip_id of found route. If no possible route is found, None is returned.
    """
    config = context['config']
    gtfs_agency = context['gtfs_agency']
    geometry_utils = context['geometry_utils']

    gtfs_route_type = [ROUTE_TYPE_TO_LOWERCASE_TAG[gtfs_route['type']],]
    if gtfs_route_type[0] == 'bus':
        # GTFS does not have trolley as a type, it is represented as a bus. Therefore, both are possible here.
        gtfs_route_type.append('trolleybus')

    print("  Processing OSM route {0} ({1})".format(osm_route.tags['name'] if 'name' in osm_route.tags else 'N/A', osm_route.id))
    # Check type
    if 'route' not in osm_route.tags:
        print("    Route type is missing for route {0}".format(osm_route.id))
    else:
        if osm_route.tags['route'] not in gtfs_route_type:
            print("    Route relation {0} type in OSM ({1}) and in GTFS ({2}) are different".format(
                osm_route.tags['name'], osm_route.tags['route'], gtfs_route_type[0]))

    # Check public_transport:version
    if 'public_transport:version' not in osm_route.tags:
        print("    public_transport:version is missing for route {0}".format(osm_route.id))
    else:
        if osm_route.tags['public_transport:version'] != "2":
            print("    public_transport:version for route {0} should be 2".format(osm_route.id))

    # Check color
    gtfs_color = gtfs_route['color'].upper()
    if gtfs_color != '' and not gtfs_color.startswith('#'):
        gtfs_color = '#{0}'.format(gtfs_color)
    if 'colour' not in osm_route.tags:
        if gtfs_color != '':
            print("    colour is missing for route {0} and it should be {1}".format(osm_route.id, gtfs_color))
        else:
            print("    colour is missing for route {0} and there is no defined colour in GTFS either".format(
                osm_route.id))
    else:
        if osm_route.tags['colour'] != gtfs_color:
            if gtfs_color != '':
                print("    Colour for route {0} type in OSM ({1}) and in GTFS ({2}) are different".format(
                    osm_route.id, osm_route.tags['colour'], gtfs_color))
            else:
                print("    Colour for route {0} type in OSM is '{1}' and colour in GTFS is not defined at all".format(
                    osm_route.id, osm_route.tags['colour']))

    # Check gtfs:route_id
    if 'gtfs:route_id' not in osm_route.tags:
        print("    gtfs:route_id is missing for route {0} and it should be {1}".format(
            osm_route.id, gtfs_route['route_id']))
    else:
        if osm_route.tags['gtfs:route_id'] != gtfs_route['route_id']:
            print("    gtfs:route_id for route {0} in OSM ({1}) and in GTFS ({2}) are different".format(
                osm_route.id, osm_route.tags['gtfs:route_id'], gtfs_route['route_id']))

    # Check gtfs:agency_name
    if 'gtfs:agency_name' not in osm_route.tags:
        print("    gtfs:agency_name is missing for route {0} and it should be {1}".format(
            osm_route.id, gtfs_agency['name']))
    else:
        if osm_route.tags['gtfs:agency_name'] != gtfs_agency['name']:
            # TODO: check if difference is only in http vs https
            print("    gtfs:agency_name for route {0} in OSM ({1}) and in GTFS ({2}) are different".format(
                osm_route.id, osm_route.tags['gtfs:agency_name'], gtfs_agency['name']))

    # Check gtfs:agency_url
    if 'gtfs:agency_url' not in osm_route.tags:
        print("    gtfs:agency_url is missing for route {0} and it should be {1}".format(
            osm_route.id, gtfs_agency['url']))
    else:
        if osm_route.tags['gtfs:agency_url'] != gtfs_agency['url']:
            print("    gtfs:agency_url for route {0} in OSM ({1}) and in GTFS ({2}) are different".format(
                osm_route.id, osm_route.tags['gtfs:agency_url'], gtfs_agency['url']))

    # Check operator
    if config.OPERATOR:
        if 'operator' not in osm_route.tags or osm_route.tags['operator'] != config.OPERATOR:
            print("    Route {0} should have operator={1}".format(osm_route.id, config.OPERATOR))

    # Check network
    if config.NETWORK:
        if 'network' not in osm_route.tags or osm_route.tags['network'] != config.NETWORK:
            print("    Route {0} should have network={1}".format(osm_route.id, config.NETWORK))

    # Check gtfs:shape_id
    gtfs_trips = gtfs_queries.get_all_gtfs_trips(context['gtfs_db'], gtfs_route['route_I'])
    if 'gtfs:shape_id' not in osm_route.tags:
        print("    gtfs:shape_id is missing for route {0}. Cannot decide which direction this is without it".format(
            osm_route.id))
    else:
        osm_shape_id = osm_route.tags['gtfs:shape_id']
        if gtfs_trips.query("shape_id=='{shape_id}'".format(shape_id=osm_shape_id)).shape[0] == 0:
            print("    gtfs:shape_id for route {0} in OSM ({1}) do not exist in GTFS trips".format(
                osm_route.id, osm_route.tags['gtfs:shape_id']))

    osm_trip_id = determine_trip_id_for_route(context, osm_route, gtfs_route, gtfs_trips)
    if not osm_trip_id:
        return None

    gtfs_trip = gtfs_trips.query("trip_id=='{0}'".format(osm_trip_id)).iloc[0]
    gtfs_stops = gtfs_queries.get_all_gtfs_stops_in_trip(context['gtfs_db'], gtfs_trip['trip_I'])
    # check from
    gtfs_from = gtfs_stops.iloc[0]['name']
    if 'from' not in osm_route.tags:
        print("    from is missing for route {0} and it should be {1}".format(osm_route.id, gtfs_from))
    else:
        osm_from = osm_route.tags['from']
        if config.NORMALIZE_OSM_NAME(osm_from) != config.NORMALIZE_GTFS_NAME(gtfs_from):
            print("    from for route {0} in OSM ({1}) and in GTFS ({2}) are different".format(
                osm_route.id, osm_from, config.NORMALIZE_GTFS_NAME(gtfs_from)))

    # Check to
    gtfs_to = gtfs_stops.iloc[-1]['name']
    if 'to' not in osm_route.tags:
        print("    to is missing for route {0} and it should be {1}".format(osm_route.id, gtfs_to))
    else:
        osm_to = osm_route.tags['to']
        if config.NORMALIZE_OSM_NAME(osm_to) != config.NORMALIZE_GTFS_NAME(gtfs_to):
            print("    to for route {0} in OSM ({1}) and in GTFS ({2}) are different".format(
                osm_route.id, osm_to, config.NORMALIZE_GTFS_NAME(gtfs_to)))

    # Check name
    if 'name' not in osm_route.tags:
        print("    Name is missing for route {0}. It should be something like '... ref: from => to".format(osm_route.id))
    else:
        m = re.match(route_name_regex, osm_route.tags['name'])
        if not m:
            print("    Name is not in proper format for route {0}. It should be something like '... ref: from => to,"
                  "and it is {1}".format(osm_route.id, osm_route.tags['name']))
        else:
            ref_part = m.groups()[0].strip()
            from_part = m.groups()[1].strip()
            to_part = m.groups()[-1].strip()
            rindex = to_part.rindex('=>')
            to_part = to_part[rindex+2:].strip()
            if 'ref' not in osm_route.tags or osm_route.tags['ref'] not in ref_part:
                print("    Name in route {0} should contain ref tag before semicolon. Ref tag either don't exist, or "
                      "name {1} is not containing it".format(osm_route.id, osm_route.tags['name']))
            if 'from' not in osm_route.tags or osm_route.tags['from'] != from_part:
                print("    Name in route {0} should have 'from' tag before '=>'. 'From' tag either don't exist, or "
                      "name {1} is not having it".format(osm_route.id, osm_route.tags['name']))
            if 'to' not in osm_route.tags or osm_route.tags['to'] != to_part:
                print("    Name in route {0} should have 'to' tag after '=>'. 'To' tag either don't exist, or "
                      "name {1} is not having it".format(osm_route.id, osm_route.tags['name']))

    # Check interval
    interval_shape_id, interval_direction_id = None, None
    if 'gtfs:shape_id' in osm_route.tags:
        interval_shape_id = osm_route.tags['gtfs:shape_id']
    else:
        interval_shape_id = gtfs_trip["shape_id"]
        if not interval_shape_id:
            interval_direction_id = gtfs_trip["direction_id"]
    most_appropriate_service = context['gtfs_service']
    interval_sec = gtfs_queries.get_gtfs_interval(
        context['gtfs_db'], gtfs_trip['route_I'], most_appropriate_service['service_I'],
        interval_shape_id, interval_direction_id)
    if not interval_sec:
        # Try again, this time without most appropriate service, to get any interval
        interval_sec = gtfs_queries.get_gtfs_interval(
            context['gtfs_db'], gtfs_trip['route_I'], service_I=None,
            shape_id=interval_shape_id, direction_id=interval_direction_id)
    if not interval_sec:
        # Try again, this time also without shape_id
        interval_sec = gtfs_queries.get_gtfs_interval(
            context['gtfs_db'], gtfs_trip['route_I'], service_I=None,
            shape_id=None, direction_id=interval_direction_id)
    if interval_sec:
        interval_osm_format = "{0:02d}:{1:02d}".format(interval_sec // 60, interval_sec % 60)
        if 'interval' not in osm_route.tags or osm_route.tags['interval'] != interval_osm_format:
            print("    Route {0} should have interval={1}".format(osm_route.id, interval_osm_format))
    else:
        print("    We either don't have shape_id or direction, or there are no departures at all."
              "Trip interval cannot be determined.")

    # Check duration
    duration_sec = gtfs_queries.get_gtfs_shape_duration(
        context['gtfs_db'], gtfs_trip['route_I'], most_appropriate_service['service_I'],
        interval_shape_id, interval_direction_id)
    if not duration_sec:
        # Try again, this time without most appropriate service, to get any duration
        duration_sec = gtfs_queries.get_gtfs_shape_duration(
            context['gtfs_db'], gtfs_trip['route_I'], service_I=None,
            shape_id=interval_shape_id, direction_id=interval_direction_id)
    if duration_sec:
        duration_osm_format = "{0:02d}:{1:02d}".format(duration_sec // 60, duration_sec % 60)
        if 'duration' not in osm_route.tags or osm_route.tags['duration'] != duration_osm_format:
            print("    Route {0} should have duration={1}".format(osm_route.id, duration_osm_format))
    else:
        print("    We either don't have shape_id or direction, or there are no departures at all."
              "Trip duration cannot be determined")

    # Check connectivity
    is_connected = check_route_connectivity(osm_route, osm_route_data)

    geom_osm_route = None
    if is_connected:  # We can get LineString of a route only if it is without gaps
        geom_osm_route = GeometryUtils.create_shape_from_osm_response(osm_route, osm_route_data)
    else:
        can_be_connected = geometry_utils.is_shape_connected(osm_route, osm_route_data)
        if can_be_connected:
            print("    While route do have gaps, it can be fixed by reordering of ways")

    if not geom_osm_route:
        print("    Cannot create shape from OSM data, some geometry checks will not be done.")

    # Check if it is roundtrip
    is_ring = False
    if is_connected:  # We can check if first and last way are the same only if route is connected
        all_ways = [w for w in osm_route.members if w.role == '' and type(w) == overpy.RelationWay]
        first_way = next(w for w in osm_route_data.ways if w.id == all_ways[0].ref)
        last_way = next(w for w in osm_route_data.ways if w.id == all_ways[-1].ref)
        if first_way.nodes[0].id == last_way.nodes[-1].id:
            is_ring = True

    if is_ring:
        if 'roundtrip' not in osm_route.tags or osm_route.tags['roundtrip'] != 'yes':
            print("    Route {0} should have roundtrip=yes".format(osm_route.id))
    else:
        if 'roundtrip' in osm_route.tags and osm_route.tags['roundtrip'] == 'yes':
            print("    Route {0} have roundtrip=yes, but its shape is not closed ring".format(osm_route.id))

    geom_gtfs_route = GeometryUtils.create_shape_from_gtfs(context['gtfs_db'], gtfs_trip["shape_id"])
    if not geom_gtfs_route:
        if 'gtfs:shape_id' in osm_route.tags:
            geom_gtfs_route = GeometryUtils.create_shape_from_gtfs(context['gtfs_db'], osm_route.tags['gtfs:shape_id'])
            if not geom_gtfs_route:
                print("    gtfs:shape_id present as route tag does not exist in GTFS")
            else:
                print("    There are no shape data present in GTFS, falling back to gtfs:shape_id tag in "
                      "route for geometry checks since that one exists")
        else:
            print("    There are no shape data present in GTFS, some geometry checks will not be done.")

    if geom_osm_route:
        GeometryUtils.check_oneway_problems(osm_route, osm_route_data)

    if geom_osm_route and geom_gtfs_route:
        hausdorff_distance = geometry_utils.get_distance_between_routes(geom_osm_route, geom_gtfs_route)
        if hausdorff_distance > config.DISTANCE_ROUTE_METERS_THRESHOLD:
            print("    Distance between shapes of OSM route and GTFS route is: {0:.1f}m. "
                  "Check shapes and fix geometry in OSM".format(hausdorff_distance))

        osm_route_length = geometry_utils.get_route_distance_in_m(geom_osm_route)
        gtfs_route_length = geometry_utils.get_route_distance_in_m(geom_gtfs_route)
        if osm_route_length > 0:
            if gtfs_route_length/float(osm_route_length) >\
                    (100 + config.TOTAL_ROUTE_DISTANCE_PERCENTAGE_THRESHOLD) / 100.0:
                print("    Total length of route in OSM ({0:.1f}m) and in GTFS ({1:.1f}m) differ too much".format(
                    osm_route_length, gtfs_route_length))
            if gtfs_route_length/float(osm_route_length) < \
                    (100 - config.TOTAL_ROUTE_DISTANCE_PERCENTAGE_THRESHOLD) / 100.0:
                print("    Total length of route in OSM ({0:.1f}m) and in GTFS ({1:.1f}m) differ too much".format(
                    osm_route_length, gtfs_route_length))

    # Check stops
    if config.CHECK_STOPS:
        process_stops(context, osm_route, osm_route_data, geom_osm_route, gtfs_stops, gtfs_route_type, geom_gtfs_route)
    return gtfs_trip['trip_id']


def process_master_route(context, osm_master_route):
    config = context['config']
    gtfs_routes = context['gtfs_routes']
    gtfs_agency = context['gtfs_agency']

    print("Processing OSM master route {0} ({1})".format(osm_master_route.id, osm_master_route.tags["name"]))
    # Try to find route_id from gtfs:route_id. If not possible, try ref.
    gtfs_route = None
    if 'gtfs:route_id' in osm_master_route.tags:
        osm_route_id = osm_master_route.tags['gtfs:route_id']
        gtfs_route = gtfs_routes.query("route_id=='{0}'".format(osm_route_id))
        if gtfs_route.shape[0] == 0:
            print("  Cannot find master route {0} in GTFS".format(osm_route_id))
            gtfs_route = None
        elif gtfs_route.shape[0] > 1:
            print("  More than one route for OSM master route {0}".format(osm_route_id))
            gtfs_route = None
    else:
        print("  gtfs:route_id missing in OSM master route {0} ({1})".format(
            osm_master_route.id, osm_master_route.tags["name"]))
        if 'ref' in osm_master_route.tags:
            print("  NOTE: Will try to use ref to get route id instead")
            osm_route_id = osm_master_route.tags['ref']
            gtfs_route = gtfs_routes.query("route_id=='{0}'".format(osm_route_id))
            if gtfs_route.shape[0] == 0:
                print("  Cannot find master route {0} in GTFS based on ref tag".format(osm_route_id))
                gtfs_route = None
            elif gtfs_route.shape[0] > 1:
                print("  More than one route for OSM master route {0} based on ref tag".format(osm_route_id))
                gtfs_route = None
    if gtfs_route is None:
        return
    # Check master route
    gtfs_route = gtfs_route.iloc[0]
    gtfs_routes.loc[(gtfs_routes.route_id == gtfs_route['route_id']), 'visited'] = True
    # Check name
    if osm_master_route.tags['name'] != gtfs_route['name']:
        print("  Route master name in OSM ({0}) and in GTFS ({1}) are different".format(
            osm_master_route.tags['name'], gtfs_route['name']))
    # Check agency
    if 'gtfs:agency_id' not in osm_master_route.tags:
        print("  gtfs:agency_id for route master {0} ({1}) is missing (it should be {2})".format(
            osm_master_route.id, osm_master_route.tags['name'], gtfs_agency['agency_id']))
    else:
        if osm_master_route.tags['gtfs:agency_id'] != gtfs_agency['agency_id']:
            print("  Route master agency_id in OSM ({0}) and in GTFS ({1}) are different".format(
                osm_master_route.tags['gtfs:agency_id'], gtfs_agency['agency_id']))
    # Check agency url
    if 'gtfs:agency_url' not in osm_master_route.tags:
        print("  agency_url for route master {0} is missing in OSM and it should be {1}".format(
            osm_master_route.id, gtfs_agency['url']))
    else:
        if osm_master_route.tags['gtfs:agency_url'] != gtfs_agency['url']:
            print("  Route master agency_id in OSM ({0}) and in GTFS ({1}) are different".format(
                osm_master_route.tags['gtfs:agency_url'], gtfs_agency['url']))
    # Check color
    gtfs_color = gtfs_route['color'].upper()
    if gtfs_color != '' and not gtfs_color.startswith('#'):
        gtfs_color = '#{0}'.format(gtfs_color)
    if 'colour' not in osm_master_route.tags:
        if gtfs_color != '':
            print("  Route master colour is missing (it should be {0})".format(gtfs_color))
        else:
            print("  Route master colour is missing and there is no defined colour in GTFS either")
    else:
        if osm_master_route.tags['colour'] != gtfs_color:
            if gtfs_color != '':
                print("  Route master colour in OSM ({0}) and in GTFS ({1}) are different".format(
                    osm_master_route.tags['colour'], gtfs_color))
            else:
                print("  Route master colour in OSM is '{0}' and colour in GTFS is not defined at all".format(
                    osm_master_route.tags['colour']))
    # Check operator
    if config.OPERATOR:
        if 'operator' not in osm_master_route.tags or osm_master_route.tags['operator'] != config.OPERATOR:
            print("  Route master {0} should have operator={1}".format(osm_master_route.id, config.OPERATOR))
    # Check network
    if config.NETWORK:
        if 'network' not in osm_master_route.tags or osm_master_route.tags['network'] != config.NETWORK:
            print("      Route master {0} should have network={1}".format(osm_master_route.id, config.NETWORK))

    gtfs_route_type = [ROUTE_TYPE_TO_LOWERCASE_TAG[gtfs_route['type']],]
    if gtfs_route_type[0] == 'bus':
        # GTFS does not have trolley as a type, it is represented as a bus. Therefore, both are possible here.
        gtfs_route_type.append('trolleybus')

    if 'route_master' not in osm_master_route.tags:
        print("  Route master type is missing (GTFS has {0})".format(gtfs_route_type))
    else:
        if osm_master_route.tags['route_master'] not in gtfs_route_type:
            print("  Route master type in OSM ({0}) and in GTFS ({1}) are different".format(
                osm_master_route.tags['route_master'], gtfs_route_type[0]))

    osm_route_data = osm_queries.get_osm_routes(context['overpass_api'], osm_master_route.id)
    gtfs_route_variants = gtfs_queries.get_all_gtfs_route_variants(context['gtfs_db'], gtfs_route['route_I'])
    gtfs_route_variants['visited'] = False

    # Visit all routes from OSM and flag that they are visited in GTFS variants
    for osm_route in sorted(osm_route_data.relations, key=lambda k: k.id):
        if 'type' not in osm_route.tags or osm_route.tags['type'] != 'route':
            print("  Invalid relation {0} (not type=route) found as child relation of route master".format(
                osm_route.id))
        found_trip_id = process_route(context, osm_route, osm_route_data, gtfs_route)
        if found_trip_id:
            gtfs_route_variants.loc[(gtfs_route_variants.trips.str.contains(found_trip_id)), 'visited'] = True

    # Report all GTFS variants not found in OSM
    # TODO: when offering sample trip, try to get one with more frequent calendar (week days vs weekends)
    for gtfs_route_variant in gtfs_route_variants.query("visited==False").iterrows():
        gtfs_stops_df = gtfs_queries.get_all_gtfs_stops_in_trip(context['gtfs_db'], gtfs_route_variant[1]['trip_I_sample'])
        print("  OSM is missing route from {0} ({1}) to {2} ({3}) with sample trip_id {4} and {5} total trips".format(
            gtfs_stops_df.iloc[0]['name'], gtfs_stops_df.iloc[0]['stop_id'],
            gtfs_stops_df.iloc[-1]['name'], gtfs_stops_df.iloc[-1]['stop_id'],
            gtfs_route_variant[1]["trip_id_sample"], gtfs_route_variant[1]["trip_count"]))


def does_service_covers_today(service):
    """
    Based on a given service, parses its start/end dates and tries to see if service is working on today's date.
    :return: True if it is covering today, False otherwise
    """
    now = datetime.datetime.now()
    start_date = datetime.datetime.strptime(service['start_date'], '%Y-%m-%d')
    end_date = datetime.datetime.strptime(service['end_date'], '%Y-%m-%d')
    return start_date <= now <= end_date


def choose_best_service(gtfs_services):
    """
    Based on a list of all services in GTFS, tries to use heuristic to choose "best" service.
    Best here means most common one operating today.
    It will first try to find services running today, and then it will try to find one that operates on most week days.
    :return: One of GTFS services
    """
    gtfs_services['covers_today'] = gtfs_services.apply(lambda row: does_service_covers_today(row), axis=1)
    gtfs_services['count_days'] =\
        gtfs_services['m'] + gtfs_services['t'] + gtfs_services['w'] + gtfs_services['th'] + \
        gtfs_services['f'] + gtfs_services['s'] + gtfs_services['su']
    # Try to find current service based on what is available today and on how much days service operates
    if gtfs_services.query("covers_today==True").shape[0] == 0:
        print("There is no service in operation for today. Will choose any other random with most days")
    gtfs_services = gtfs_services.sort_values(by=['covers_today', 'count_days'], ascending=[False, False])
    return gtfs_services.iloc[0]


def create_global_context():
    parser = argparse.ArgumentParser(
        description='GTFS-OSM Validator - helper tool to find errors and differences in OSM from GTFS data')
    parser.add_argument('gtfs', default='gtfs.zip',
                        help='Path to input gtfs zip file. Default is "gtfs.zip"')
    parser.add_argument('--output-sqlite-file', default='gtfs.sqlite',
                        help='Path to output SQLite file where GTFS will be stored for processing. '
                             'Default is "gtfs.sqlite"')
    parser.add_argument('--osm-route-master', default=None, type=int,
                        help="Process only this OSM route master. Useful if you want to focus on only one master route")
    parser.add_argument('--config', default='belgrade', type=str,
                        help="Configuration file as python module. Default is belgrade")
    parser.add_argument('-v', '--version', action='version', version='GTFS-OSM Validator 0.1')

    args = parser.parse_args()

    if not os.path.isfile(args.gtfs):
        error_msg = 'GTFS file {0} is missing.'.format(args.gtfs)
        parser.error(error_msg)

    config_file = f"./src/configs/{args.config}.py"
    if not os.path.isfile(config_file):
        error_msg = 'Config file {0} is missing'.format(config_file)
        parser.error(error_msg)

    gtfs_db = load_or_import_example_gtfs(args.gtfs, args.output_sqlite_file)
    config = importlib.import_module('.' + args.config, 'configs')

    overpass_api = overpy.Overpass(url=config.OVERPASS_API_SERVER)
    gtfs_routes = gtfs_queries.get_all_gtfs_routes(gtfs_db)
    gtfs_agency = gtfs_queries.get_gtfs_agency(gtfs_db, config.AGENCY_ID)
    gtfs_services = gtfs_queries.get_gtfs_services(gtfs_db)
    gtfs_service = choose_best_service(gtfs_services)
    geometry_utils = GeometryUtils(*gtfs_queries.get_stops_bounding_box(gtfs_db))
    return {
        'config': config,
        'gtfs_db': gtfs_db,
        'overpass_api': overpass_api,
        'gtfs_routes': gtfs_routes,
        'gtfs_agency': gtfs_agency,
        'gtfs_services': gtfs_services,
        'gtfs_service': gtfs_service,
        'filter_osm_route_master': args.osm_route_master,
        'geometry_utils': geometry_utils
    }


def main():
    context = create_global_context()
    config = context['config']

    if config.ROUTE_MASTER_QUERY_METHOD == 'agency_name':
        osm_master_routes = osm_queries.get_all_osm_master_routes_using_agency_name(
            context['overpass_api'], context['gtfs_agency']["name"])
    elif config.ROUTE_MASTER_QUERY_METHOD == 'operator':
        if not config.ROUTE_MASTER_OPERATOR:
            print("ERROR: If you define operator as query method, you need to define it in ROUTE_MASTER_OPERATOR")
            return
        osm_master_routes = osm_queries.get_all_osm_master_routes_using_operator(
            context['overpass_api'], config.ROUTE_MASTER_OPERATOR)
    elif config.ROUTE_MASTER_QUERY_METHOD == 'network':
        if not config.ROUTE_MASTER_NETWORK:
            print("ERROR: If you define network as query method, you need to define it in ROUTE_MASTER_NETWORK")
            return
        osm_master_routes = osm_queries.get_all_osm_master_routes_using_network(
            context['overpass_api'], config.ROUTE_MASTER_NETWORK)
    else:
        print("ERROR: Unknown ROUTE_MASTER_OPERATOR specified")
        return

    context['gtfs_routes']['visited'] = False
    for osm_master_route in sorted(osm_master_routes, key=lambda k: k.id):
        if not 'type' in osm_master_route.tags or not osm_master_route.tags['type'] == 'route_master':
            continue
        if context['filter_osm_route_master']:
            if osm_master_route.id == context['filter_osm_route_master']:
                process_master_route(context, osm_master_route)
        else:
            process_master_route(context, osm_master_route)

    if not context['filter_osm_route_master']:
        # There is no filtered routes, so list all routes that were not processed in this execution
        for gtfs_route in context['gtfs_routes'].query("visited==False").iterrows():
            print("OSM is missing route {0} ({1})".format(gtfs_route[1]["name"], gtfs_route[1]["long_name"]))

    # Check all stops that have proper operator tag, but there are no route with that operator
    if not context['filter_osm_route_master'] and config.CHECK_STOPS and config.OPERATOR != '':
        osm_non_operator_stops = osm_queries.get_all_non_operator_stops(context['overpass_api'], config.OPERATOR)
        for osm_stop in osm_non_operator_stops.nodes:
            print("Stop {0} ({1}) has 'operator={2}' while there is no a single route with "
                  "'operator={2}' associated on that stop".format(
                osm_stop.tags['name'] if 'name' in osm_stop.tags else '', osm_stop.id, config.OPERATOR))


if __name__ == '__main__':
    main()
